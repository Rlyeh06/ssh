#! /usr/bin/env python
# -*- encoding: utf-8 -*-
import os
import sys
import socket
import select
import datetime
import time
from socket import gethostbyname_ex, gethostname
from Crypto.PublicKey import RSA
from Crypto.Util import randpool
import pickle
import getpass

Host = ["192.168.1.19"]
forlist = range(len(Host))
nb_open = 0
num = 4096
error = "/!\ Error remote shell /!\ \n"
phrase = "je suis le client"
SEPARATOR = "__________" #utiliser pour la signature des paquets
clisock = []

def identification(self):
	i = self
	identifier = False
	cpt = 0
	while (identifier == False):
		mdp = getpass.getpass('Entrer mot de passe serveur (ici le mot de passe est test)'+Host[i]+':')
		date = datetime.datetime.now().strftime("%s")
		mdps = mdp + date
		envoyer(mdps, clisock[i])
		part = int(recevoir(num, 1, clisock[i]))
		data = recevoir(num, part, clisock[i])
		if data == "ok\n":
			time.sleep(1)
			identifier = True
		time.sleep(2)
		cpt = cpt + 1
		if cpt > 4:
			print "#############################"
			print "#                           #"
			print "# /!\ trop de connexion /!\ #"
			print "#                           #"
			print "#############################\n"
			time.sleep(2)
			identifier = "Break"
			Host.pop(i)
			clisock[i].close()
			clisock.pop(i)
			sys.exit(0)

def connecte(self, i):
	j = 0
	Port = self
	while(j <= 1001):
		try:
			print "#######################################"
			print "#                                     #"
			print "# Connecté à:", Host[i], "port :", Port, "#"
			print "#                                     #"
			print "#######################################\n"
			addr = (Host[i], Port)
			clisock[i].connect(addr)
			mot = clisock[i].recv(num)
			if (mot != "je suis le serveur"):
				print "######################################"
				print "#                                    #"
				print "# /!\ Pas de serveur sur ce port /!\ #"
				print "#                                    #"
				print "######################################\n"
				clisock[i].close()
				j += 1
				Port += 1
				pass
			clisock[i].send(phrase)
			recoicle(clisock[i])
			envoiecle(clisock[i])
			identification(i)
			break
		except socket.error:
			j += 1
			Port += 1
			if (j == 1001):
				print error
				raw_input()
	j = 0

def decrypt(self):
	serveur_key_full_file = open("./.zz-client-full.key", 'r')
	serveur_key_full = pickle.load(serveur_key_full_file)
	decrypted_message = serveur_key_full.decrypt(self)
	return decrypted_message

def signed(self):
	serveur_key_full_file = open("./.zz-client-full.key", 'r')
	serveur_key_full = pickle.load(serveur_key_full_file)
	signed_message = serveur_key_full.sign(self, None)[0]
	return signed_message

def encrypt(self, sock):
	client_key_file= open("./.zz-serveur-public"+str(sock)+".key", 'r')
	client_key = pickle.load(client_key_file)
	encrypted_message = client_key.encrypt(self, None)[0]
	return encrypted_message

def verif(self, message, sock):
	serveur_key_file= open("./.zz-serveur-public"+str(sock)+".key", 'r')
	serveur_key = pickle.load(serveur_key_file)
	(response, signature_str) = self.split(SEPARATOR)
	signature = long(signature_str)
	result = serveur_key.verify(message, (signature, ))
	#result = serveur_key.verify(message[1:2], (signature, )) pour verifier que la verification de signature marche bien
	if (result == False):
		print "###########################"
		print "#                         #"
		print "# /!\ Paquets altérés /!\ #"
		print "#                         #"
		print "###########################"
	return result

def generationcle():
	print "################################"
	print "#                              #"
	print "# Génération des clés en cours #"
	print "#                              #"
	print "################################\n"
	pool = randpool.RandomPool()
	key = RSA.generate(1024, pool.get_bytes)
	pickle.dump(key, open("./.zz-client-full.key", 'w'))
	pickle.dump(key.publickey(), open("./.zz-client-public.key", 'w'))
	print "#################"
	print "#               #"
	print "# Clés générées #"
	print "#               #"
	print "#################\n"

def envoiecle(self):
	print "######################"
	print "#                    #"
	print "# Envoi Clé en cours #"
	print "#                    #"
	print "######################\n"
	fich = open("./.zz-client-public.key", "rb")
	donnees = fich.read()
	self.send(donnees)
	fich.close()
	print "###############"
	print "#             #"
	print "# Clé envoyée #"
	print "#             #"
	print "###############\n"

def recoicle(self):
	print "##########################"
	print "#                        #"
	print "# Réception Clé en cours #"
	print "#                        #"
	print "##########################\n"
	fichier = open("./.zz-serveur-public"+str(self)+".key", "w")
	data = self.recv(num)
	fichier.write(data)
	fichier.close()
	print "#############"
	print "#           #"
	print "# Clé reçut #"
	print "#           #"
	print "#############\n"

def envoyer(self,sock):
	message = encrypt(self, sock)
	signed_message = signed(self)
	data = message+SEPARATOR+str(signed_message)
	sock.send(data)

def recevoir(self, n, sock):
	texte = ""
	while (n != 0):
		data = sock.recv(self)
		(message_encrypted, signature_str) = data.split(SEPARATOR)
		message = decrypt(message_encrypted)
		texte = texte + message
		result = verif(data, message, sock)
		n = n - 1
	try:
		x = int(texte)
	except ValueError:
		for i in xrange(len(clisock)):
			if sock == clisock[i]:
				head = Host[i]
		print str(head) + ":\n" + texte
	return texte

def version():
	print "\n"
	print " ____       _     _         _____             _                         "
	print "|  _ \ ___ | |__ (_)_ __   |  ___|_ _ ___ ___(_)_ __   __ _             "
	print "| |_) / _ \| '_ \| | '_ \  | |_ / _` / __/ __| | '_ \ / _` |            "
	print "|  _ < (_) | |_) | | | | | |  _| (_| \__ \__ \ | | | | (_| |            "
	print "|_| \_\___/|_.__/|_|_| |_| |_|  \__,_|___/___/_|_| |_|\__,_|            "
	print "                                                                        "
	print "                   *********************************                    "
	print "                   * Projet Systeme d’exploitation *                    "
	print "                   *   Remote Shell Python v.1.0   *                    "
	print "                   *            05/2013            *                    "
	print "                   *********************************                    \n"


def help():
	print "****************************************************************************"
	print "* Help:                                                                    *"
	print "* Taper Help|help|-h pour afficher cette aide                              *"
	print "* Taper login puis une adresse ip pour se connecter à un autre serveur     *"
	print "* Taper logout puis une adresse ip pour se déconnecter d’un serveur précis *"
	print "* Taper logout puis all pour se déconnecter de tout les serveurs           *"
	print "* Taper Version pour afficher les informations sur remote shell            *"
	print "* Sinon taper tout simpement une commande                                  *"
	print "****************************************************************************"
	raw_input("Appuyer sur entrer pour retourner au remote shell")

help() #affiche l’help au démarrage
generationcle()

for x in forlist:
	sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM, 0)
	clisock.append(sock)
	Port = 1500
	connecte(Port, x)
	nb_open += 1

while nb_open > 0:
	first = False
	cmd = raw_input("user@bt:~$ ")
	test = cmd.split()
	if (test[0] == "Help") or (test[0] == "help") or (test[0] == "-h"):
		help()
	elif (test[0] == "easter-egg") or (test[0] == "Easter-egg"):
		pid = os.fork()
		if pid == 0:
			os.execv('/bin/sh',('sh', '-c','xterm -e telnet towel.blinkenlights.nl'))
	elif (test[0] == "Version") or (test[0] == "version") or (test[0] == "-v"):
		version()
	elif test[0] == "login":
		if (len(test) == 1):
			print "Merci de préciser l’ip du serveur où se connecter\n"
		else:
			Host.append(test[1])
			sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM, 0)
			clisock.append(sock)
			connecte(Port, len(Host)-1)
			nb_open += 1
	elif test[0] == "logout":
		if (len(test) == 1):
			print "Merci de préciser l’ip du serveur ou all pour tout fermer\n"
		elif test[1] == "all":
			for i in xrange(len(Host)):
				envoyer(test[0], clisock[i])
				part = int(recevoir(num, 1, clisock[i]))
				message = recevoir(num, part, clisock[i])
				time.sleep(0.5)
				Host = []
				nb_open = 0
		else:
			X = 0
			delete = False
			while (x < len(Host)):
				if test[1] == Host[x]:
					envoyer(test[0], clisock[x])
					part = int(recevoir(num, 1, clisock[x]))
					message = recevoir(num, part, clisock[x])
					time.sleep(0.5)
					Host.pop(x)
					clisock.pop(x)
					nb_open -= 1
					delete = True
					print "######################"
					print "#                    #"
					print "# serveur déconnecté #"
					print "#                    #"
					print "######################\n"
				x += 1
			if (delete == False):
				print "########################"
				print "#                      #"
				print "# serveur non connecté #"
				print "#                      #"
				print "########################\n"
	else:
		for i in xrange(len(Host)):
			testcmd = cmd.count('&')
			if (testcmd != 0):
				while (testcmd != -1):
					envoyer(cmd, clisock[i])
					part = int(recevoir(num, 1, clisock[i]))
					message = recevoir(num, part, clisock[i])
					testcmd -= 1
					time.sleep(2)
			else:
				envoyer(cmd, clisock[i])
				part = int(recevoir(num, 1, clisock[i]))
				message = recevoir(num, part, clisock[i])
				time.sleep(2)
print "##################################################"
print "#                                                #"
print "# /!\ Dernière connection fermée. Au revoir! /!\ #"
print "#                                                #"
print "##################################################\n"
sys.exit(0)
