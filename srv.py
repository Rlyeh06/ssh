#! /usr/bin/env python
# -*- encoding: utf-8 -*-
import os
import sys
import socket
import select
import datetime
import time
from socket import gethostbyname_ex, gethostname
from Crypto.PublicKey import RSA
from Crypto.Util import randpool
import pickle

connsock = socket.socket(socket.AF_INET, socket.SOCK_STREAM, 0)
Host = ""
port = 2500
error = "error rsh"
nb_open = 0
# Create list of potential readers and place connection socket in first position
potential_readers = [connsock]
BUFSIZE = 99999
white_list = ['ls', 'grep', 'ps', 'info', 'apropos', 'clear']
SEPARATOR = "__________"
num = 4096
phrase = "je suis le serveur"
i=0
while(i<1000):
    try:
        serveraddr = (Host, port)
        connsock.bind(serveraddr)
        QUEUE_SIZE = 5
        connsock.listen(QUEUE_SIZE)
        print "#######################################"
        print "#                                     #"
        print "# Serveur attend sur le port ",port,"   #"
        print "#                                     #"
        print "#######################################\n"
        break
    except socket.error:
        i += 1
        port -= 1
        if (i == 999): 
            print error
            raw_input("Enter pour continuer")
debutstr = datetime.datetime.now().strftime("%s")
debut = int(debutstr)

def decrypt(self):
	serveur_key_full_file = open("./.zz-serveur-full.key", 'r')
	serveur_key_full = pickle.load(serveur_key_full_file)
	decrypted_message = serveur_key_full.decrypt(self)
	return decrypted_message

def signed(self):
	serveur_key_full_file = open("./.zz-serveur-full.key", 'r')
	serveur_key_full = pickle.load(serveur_key_full_file)
	signed_message = serveur_key_full.sign(self, None)[0]
	return signed_message

def encrypt(self, sock):
	client_key_file= open("./.zz-client-public"+str(sock)+".key", 'r')
	client_key = pickle.load(client_key_file)
	encrypted_message = client_key.encrypt(self, None)[0]
	return encrypted_message

def verif(self, message, sock):
	serveur_key_file= open("./.zz-client-public"+str(sock)+".key", 'r')
	serveur_key = pickle.load(serveur_key_file)
	(response, signature_str) = self.split(SEPARATOR)
	signature = long(signature_str)
	result = serveur_key.verify(message, (signature, ))
	#result = serveur_key.verify(message[1:2], (signature, )) pour verifier que la verification de signature marche bien
	if (result == False):
		print "###########################"
		print "#                         #"
		print "# /!\ Paquets altérés /!\ #"
		print "#                         #"
		print "###########################\n"
	return result

def generationcle():
	print "###################################"
	print "#                                 #"
	print "# Génération des clés en cours... #"
	print "#                                 #"
	print "###################################\n"
	pool = randpool.RandomPool()
	key = RSA.generate(1024, pool.get_bytes)
	pickle.dump(key, open("./.zz-serveur-full.key", 'w'))
	pickle.dump(key.publickey(), open("./.zz-serveur-public.key", 'w'))
	print "#################"
	print "#               #"
	print "# clés générées #"
	print "#               #"
	print "#################\n"

def envoiecle(self):
	print "###############################"
	print "#                             #"
	print "# Envoi Clé publique en cours #"
	print "#                             #"
	print "###############################\n"
	fich = open("./.zz-serveur-public.key", "rb")
	donnees = fich.read()
	self.send(donnees)
	fich.close()
	print "###############"
	print "#             #"
	print "# Clé envoyée #"
	print "#             #"
	print "###############\n"

def recoicle(self):
	print "###################################"
	print "#                                 #"
	print "# Réception Clé publique en cours #"
	print "#                                 #"
	print "###################################\n"
	fichier = open("./.zz-client-public"+str(self)+".key", "w")
	data = self.recv(num)
	fichier.write(data)
	fichier.close()
	print "#############"
	print "#           #"
	print "# Clé reçut #"
	print "#           #"
	print "#############\n"

def identification(self,sock):
	data = sock.recv(num)
	date = datetime.datetime.now().strftime("%s")
	(message_encrypted, signature_str) = data.split(SEPARATOR)
	decrypted_message = decrypt(message_encrypted)
	result = verif(data, decrypted_message, sock)
	date1 = decrypted_message[-10:]
	dates = int(date)/10
	dates1 = int(date1)/10
	mdp = decrypted_message[:-10]
	if (mdp == "test" and dates1 == dates):
		envoyer("ok\n",sock)
		return True
	else:
		envoyer("false\n",sock)
		if self > 4:
			print "#############################"
			print "#                           #"
			print "# /!\ trop de connexion /!\ #"
			print "#                           #"
			print "#############################\n"
			return "break"
		return False

def commande(self,sock):
	test = self.split()
	if (test[0] in white_list):
		accepted = True
		testcmd = self.count('&')
		testcmd1 = self.count('|')
		if (testcmd1 > 0):
			test1 = self.split('|')
			i = 0
			while (testcmd1 != -1):
				test3 = test1[i].strip()
				test5 = test3.split()
				if (test5[0] in white_list):
					testcmd1 = testcmd1 - 1
					i = i + 1
				else: 
					envoyer("commande interdite: %s\n" % (test3),sock)
					accepted = False
					testcmd1 = testcmd1 - 1
					i = i + 1
		if (testcmd > 0):
			test2 = self.split('&')
			i = 0
			while (testcmd != -1):
				test4 = test2[i].strip()
				test6 = test4.split()
				if (test6[0] in white_list):
					testcmd = testcmd - 1
					i = i + 1
				else: 
					envoyer("commande interdite: %s\n" % (test4),sock)
					accepted = False
					testcmd = testcmd - 1
					i = i + 1
			if (accepted == True):
				testcmd = self.count('&')
				i = 0
				while (testcmd != -1):
					data = executer(test2[i])
					envoyer(data,sock)
					i += 1
					testcmd -= 1
					time.sleep(1)
				accepted = False
		if (accepted == True):
			data = executer(self)
			envoyer(data,sock)
	else:
		envoyer("commande interdite: %s\n" % (test[0]),sock)

def executer(self):
	(rfather,wchild) = os.pipe() #tube fils->pere
	pid = os.fork()
	if pid == 0:#fils
		os.close(rfather)#le fils ne lit pas
		os.dup2(wchild, sys.stdout.fileno())
		os.execv('/bin/sh',('sh','-c',self))
		os.close(wchild)
		sys.exit(0)
	else:#pere
		os.close(wchild)#le pere n ecrit pas
		os.wait()
		res = os.read(rfather,BUFSIZE)
		os.close(rfather)
		return res

def envoyer(self,sock):
	message = self
	size = 64
	part = int(len(message) / size) + 1
	encrypted_message = encrypt(str(part),sock)
	signed_message = signed(str(part))
	data = encrypted_message+SEPARATOR+str(signed_message)
	sock.send(data)
	i = 0
	j = size
	while (part != 0):
		message = self[i:j]
		encrypted_message = encrypt(message, sock)
		signed_message = signed(message)
		data = encrypted_message+SEPARATOR+str(signed_message)
		sock.send(data)
		part = part - 1
		i = i + size
		j = j + size
		time.sleep(0.25)

def recevoir(self,nume):
	data = self.recv(nume)
	(message_encrypted, signature_str) = data.split(SEPARATOR)
	message = decrypt(message_encrypted)
	result = verif(data, message,self)
	return message

generationcle()
first = True
while first or nb_open > 0:
	datestr = datetime.datetime.now().strftime("%s")
	date = int(datestr)
	temps = date - debut
	first = False
	readers,writers,errors = select.select(potential_readers,[],[],1600)
	if temps > 1200:
		for sock_ready in readers:
			msg = "Logout. Temps dépassé...\n"
			envoyer(msg,sock_ready)
			print msg
			sock_ready.close()
			# Remove the closed connection from potential readers
			for i in xrange(1,len(potential_readers)):
				if potential_readers[i] == sock_ready:
					potential_readers.pop(i)
			nb_open -= 1
		break
	for sock_ready in readers:
		if sock_ready == connsock: #new connection
			conn,(addr,port) = connsock.accept()
			conn.settimeout(1600)
			conn.send(phrase)
			ident = conn.recv(num)
			if (ident != "je suis le client"):
				pass
			print "#############################################################"
			print "#                                                           #"
			print "# /!\ Connexion entrante de %s sur port %d... /!\ #"%(addr,port)
			print "#                                                           #"
			print "#############################################################\n"
			identifier = False
			cpt = 0
			envoiecle(conn)
			recoicle(conn)
			print "###########################"
			print "#                         #"
			print "# Identification en cours #"
			print "#                         #"
			print "###########################\n"
			while identifier == False:
				identifier = identification(cpt,conn)
				cpt = cpt + 1
			if identifier == "break":
				conn.close()
			if identifier == True:
				print "#############"
				print "#           #"
				print "# Identifié #"
				print "#           #"
				print "#############\n"
				potential_readers.append(conn)
				nb_open+=1
		else: #commande
			msg = recevoir(sock_ready, num)
			if (msg == "logout"):
				msg = "Logout. Fermeture connection...\n"
				envoyer(msg,sock_ready)
				print "###################################"
				print "#                                 #"
				print "# Logout. Fermeture connection... #"
				print "#                                 #"
				print "###################################\n"
				sock_ready.close()
				# Remove the closed connection from potential readers
				for i in xrange(1,len(potential_readers)):
					if potential_readers[i] == sock_ready:
						potential_readers.pop(i)
				nb_open -= 1
			else: 
				commande(msg,sock_ready)
connsock.close()
print "##################################################"
print "#                                                #"
print "# /!\ Dernière connection fermée. Au revoir! /!\ #"
print "#                                                #"
print "##################################################\n"
sys.exit(0)
